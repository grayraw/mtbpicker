import {HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { QuizPage } from '../components/quiz'

@Injectable()
export class DataService {
    bikes: Array<any>;
    // params: HttpParams
    constructor(
        private quiz: QuizPage,
        // public handler: HttpHandler
        public http: HttpClient,
        // private params: HttpParams
    ){
    }
    getBikes(){
        //api keyEFHFYk5j24GJLt

        const params = new HttpParams()
            .set('api_key', 'keyEFHFYk5j24GJLt')
            .set('maxRecords', '50')
            .set('filterByFormula', "NOT({Bike} = '')");

        return this.http.get('https://api.airtable.com/v0/app3KArSP9hNIrLkU/Bikes', {params})
        // return [];
    }
    getTopFive(){
        
        // if(!set) formula = "NOT({Bike} = '')";

        // let formula = 

        const params = new HttpParams()
            .set('api_key', 'keyEFHFYk5j24GJLt')
            .set('maxRecords', '50')
            // .set('filterByFormula', formula);

        return this.http.get('https://api.airtable.com/v0/app3KArSP9hNIrLkU/Bikes', {params})
    }

}