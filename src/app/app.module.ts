import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { FormsModule } from '@angular/forms';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
// import { SafePipeModule } from 'safe-pipe';
import { InlineSVGModule } from 'ng-inline-svg';


import { AppComponent } from './app.component';
import { SlideComponent } from './components/slide-frame';
import { QuizPage } from './components/quiz';
import { ResultsPage } from './components/results';
import { BikeList } from './components/bike-list';
import { BikePage } from './components/bike';
// import { DataComponent } from './components/data.component';
import { DataService } from './data/data-service'


const appRoutes: Routes = [
  { path: '', component: QuizPage },
  { path: 'results', component: ResultsPage },
  { path: 'bikes', component: BikeList },
  { path: 'bike/:id', component: BikePage },
  // {
  //   path: 'heroes',
  //   component: HeroListComponent,
  //   data: { title: 'Heroes List' }
  // }
  // { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    SlideComponent,
    QuizPage,
    ResultsPage,
    BikeList,
    BikePage
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule, BrowserAnimationsModule, IonRangeSliderModule, FormsModule, Ng2GoogleChartsModule,
    InlineSVGModule, HttpClientModule
    // SafePipeModule
  ],
  providers: [DataService, QuizPage],
  bootstrap: [AppComponent]
})
export class AppModule { }
