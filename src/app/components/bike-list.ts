import { Component } from '@angular/core';
import {DataService} from '../data/data-service';
import {Router} from '@angular/router';

//quote should be random everytime

@Component({
    selector: 'bike-list',
    template: `
    <div *ngFor="let bike of dataService.bikes; index as i" bike-container (click)="this.router.navigateByUrl('/bike/' + bike.id)"> 

            <div position>#{{i + 1}}</div>
            <div pic><img [src]="bike.fields.Image[0].url"></div>
            <div name>{{bike.fields.Bike}}</div>
            <div brand><img [src]="bike.fields.Brand"></div>
            <div score><span>{{bike.score}}</span></div>
            <div quote>{{bike.quote}}</div>

    </div>
    `
})
export class BikeList {
    // bikes: any;
    constructor(
        private dataService: DataService,
        private router: Router
    ){
        this.dataService.getBikes().subscribe((response:any)=>{
            // console.log(response)
            this.dataService.bikes = response.records
        });
    }
}