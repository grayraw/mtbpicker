import { ComponentFixture, TestBed, ComponentFixtureAutoDetect, async } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { SlideComponent } from './slide-frame';
import { QuizPage } from './quiz';
import { InlineSVGModule } from 'ng-inline-svg';
import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { FormsModule } from '@angular/forms';

describe('BannerComponent (inline template)', () => {
    
      let comp:    SlideComponent;
      let fixture: ComponentFixture<SlideComponent>;
      let de:      DebugElement;
      let el:      HTMLElement;
      let slides:  any;
    
    
        beforeEach( async( () => {
            TestBed.configureTestingModule({
                declarations: [ SlideComponent, QuizPage ], // declare the test component
                providers: [
                    { provide: ComponentFixtureAutoDetect, useValue: true }
                ], 
                imports: [InlineSVGModule, IonRangeSliderModule, FormsModule]
            })
            .compileComponents();

        }) );

        beforeEach(() => {

            fixture = TestBed.createComponent(SlideComponent);
            comp = fixture.componentInstance; // BannerComponent test instance

            // query for the title <h1> by CSS element selector
            // de = fixture.debugElement.query(By.css('h1'));
            // el = de.nativeElement;
            fixture.detectChanges();
        });

        it('should move slide up', () => {
            // comp.slideUp();
            expect(true).toBe(true);
        });

    });

