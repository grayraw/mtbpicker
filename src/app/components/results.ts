import {Component} from '@angular/core';
import {Router} from '@angular/router';
import { DataService } from './../data/data-service';

@Component({
    selector: 'results',
    template: `
        <div results-container>
            <div first-place>
                <div position>
                    #1
                </div>
                <div bike-image>
                    <img [src]="firstBike.fields.Image[0].url">
                    <div score>{{firstBike.fields.Score}}</div>
                </div>
                <div bike-info>
                    <div breadcrumbs></div>
                    <h1>{{firstBike.fields.Bike}}</h1>
                    <div (click)="this.router.navigateByUrl('/bike/' + firstBike.id)">Read more</div>
                </div>
            </div>
            <div runners-up>
                <div *ngFor="let bike of runnersUp; i as index" (click)="this.router.navigateByUrl('/bike/' + bike.id)">
                    <div position>
                        {{i + 2}}
                    </div>
                    <div bike-image>
                        <img [src]="bike.fields.Image[0].url">
                        <div score>{{bike.fields.Score}}</div>
                    </div>
                    <div bike-info>
                        <h1>{{bike.fields.Bike}}</h1>
                    </div>
                </div>
            </div>
        </div>
    `,
    styles: []
})
export class ResultsPage {
    constructor(
        private router: Router,
        private data: DataService
    ){
        // console.log(quiz.selection)
    }

    ngOnInit(){
        this.data.getTopFive().subscribe( response =>{
            console.log(response)
        }, error => { console.log(error) })
    }
}