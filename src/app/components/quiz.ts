import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { SlideComponent } from './slide-frame';

@Component({
    selector: 'quiz',
    templateUrl: 'quiz.html'
})
export class QuizPage{
    @ViewChild(SlideComponent) slides: any;
    // maxPrice: number = 1000;
    public selection: any = {
        type: 'basic',
        carbonFrame: false,
        dropperPost: false,
        oneChainring: false,
        plusTires: false,
        maxPrice: 1000
    };
    // frameSelected: boolean = false;
    // postSelected: boolean = false;
    // systemSelected: boolean = false;
    // tyresSelected: boolean = false;
    constructor(
        private router: Router
    ){
        //waiting for a continent and navigating to results after that
        // window.addEventListener('emitContinent', (event)=>{
        //   this.selection.continent = event.detail
        //   this.router.navigateByUrl('/bikes')
        // })
    }

    OnPriceChange(e){
    // console.log(e)
        this.selection.maxPrice = e.from;
    }
    changeSlide(){
        this.slides.slideDown();
    }

    showResults(){
        this.router.navigateByUrl('/results');
    }

    goToBikes(){
        this.router.navigateByUrl('/bikes');
    }
}