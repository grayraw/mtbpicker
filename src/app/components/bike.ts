import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {DataService} from '../data/data-service';

@Component({
    selector: 'bike',
    template: `
        <div bike-container>
            <div close (click)="location.back()">x</div><div logo>Pick a bike</div>
            <div bike>
                <img [src]="bike.Image[0].url">
                <div blurb>
                    <small breadcrumbs></small>
                    <h1>Title</h1>
                    <div score>86</div>
                    <div market>Amazon</div>
                </div>
            </div>
            <div specs-quotes>
                <div quotes></div>
                <div specs>
                    <div>Frame:</div>
                    <div>Carbon</div>
                </div>
            </div>
        </div>
    `, 
    styleUrls: ['./bike.scss']
})
export class BikePage{
    bike: Array<any>;
    id: string = '';
    constructor(
        private router: Router,
        private dataService: DataService,
        private currentRoute: ActivatedRoute,
        private location: Location
    ){
        if (!dataService.bikes) {
            this.dataService.getBikes().subscribe((response:any)=>{
                this.dataService.bikes = response.records;
                this.getId();
            });
        } else {
            this.getId();
        }
    }

    getId(){
        this.currentRoute.url.subscribe((data)=>{
            this.id = data[1].path;

            let index = Object.keys(this.dataService.bikes).filter( (key)=>{
                return this.dataService.bikes[key].id === this.id
            } )
            this.bike = this.dataService.bikes[index[0]].fields;
            console.log(this.bike)
        })
    }
}
