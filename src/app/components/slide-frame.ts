import { Component, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { trigger, state, transition, animate, style } from '@angular/animations';


@Component({
    // animations: [
    //     trigger('bounceLeft', [
    //         state('center', style({opacity: 0})),
    //         state('left', style({opacity: 1})),
    //         transition('center => left', [
    //             animate("1s")
    //         ])
    //     ])
    // ],
    selector: 'slides',
    template: `
        <div arrow-up (click)="slideUp()" [inlineSVG]="'assets/img/arr.svg'" [hidden] = 'slideVisible === 0' ></div>
            <div slide-container (nextSlide)="slideRight()">
                <div slide-belt >
                        <ng-content></ng-content>
                </div>
            </div>
        <div arrow-down (click)="slideDown()" [inlineSVG]="'assets/img/arr.svg'" [hidden] = 'slideVisible === slides.length - 1'></div>
    `,
    styles: [
        '[slide-container] {overflow: hidden}',
        ':host /deep/ [slide] {height: 400px; transition: all .3s ease;}',
        '[arrow-down] {transform: rotateX(180deg)}'
    ]
})
export class SlideComponent {
    // status: any;
    // @ViewChild('slides') slides;
    slides: Array<any> = [];
    window: any;
    slideVisible: number = 0;
    @Output() lastSlideReached: EventEmitter<any> = new EventEmitter();
    constructor(
        // private ngZone:NgZone,
        // private appRef: ApplicationRef, 
        private elementSlides: ElementRef
    ){

    }
    slideUp(){
        this.slideVisible -= 1;
        this.slides.forEach(function(el){

            el.style.height = 0;
            el.style.opacity = 0;
        })
        this.slides[this.slideVisible].style.opacity = 1;
        this.slides[this.slideVisible].style.height = '400px';
        console.log(this.slides);
    }
    slideDown(){
        this.slideVisible += 1;
        this.slides.forEach(function(el){

            el.style.opacity = 0;
            el.style.height = 0;
        })
        this.slides[this.slideVisible].style.opacity = 1;
        this.slides[this.slideVisible].style.height = '400px';
    }

    ngAfterViewInit(){
        this.slides = this.elementSlides.nativeElement.querySelectorAll('[slide]');
        this.slides.forEach(function(el){

            el.style.opacity = 0;
            el.style.height = 0;
            el.style.overflow = 'hidden';
        })
        this.slides[this.slideVisible].style.opacity = 1;
        this.slides[this.slideVisible].style.height = '400px';
    }

}